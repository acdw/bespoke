# BESPOKE / WM

It's a namespaced (probably not very efficient) window manager built with [wmutils](https://github.com/wmutils) and shell scripts.

# INVOCATION

    ln -s wm $PATH/bespoke

and in ~/.xinitrc:

    exec bespoke

Then, to do things IN the X session, use `wm`:

  wm <cmd> <args...>

Where `cmd` is an executable script in `bin/`.

# WHY?

I don't know.  I'm bored.

# LICENSE

WTFPL.  Sure, why not.

(c) Case Duckworth 2017
