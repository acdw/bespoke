#!/bin/sh
# wm functions

_runf () # Run a function with a wid or something that'll get to a wid
{ # _runf <win> | <cmd> [args...]
  f="$1"; shift || die "$E_ARG" "_runf: Not enough arguments";
  case "$1" in
    0x*) wid="$1" ;;
    *)
      cmd="$BIN/$1"; shift || die $E_ARG "_runf: Not enough arguments";
      if [ -x "$cmd" ]; then
        wid="$($cmd "$@")";
      else
        die $E_EXE "${0}: _runf: Unknown command: \"$cmd\"";
      fi
      ;;
  esac
  expr "$wid" : '0x[0-9A-Fa-f]\+' >/dev/null 2>&1 || {
    die $E_WIN "${0}: _runf: Bad window \"$wid\"";
  }
  $f $wid;
  unset f wid;
}

# Atomic window-management actions
# Each of these takes one wid and does something with it using
# the _runf function above.
wm_focus () # Focus a window
{
  focus_wid () {
    if wattr "$1"; then
      wtf "$1";
    fi
  }
  _runf focus_wid "$@";
}

wm_map () # Map a window
{
  map_wid () {
    if wattr "$1" && ! wattr m "$1"; then
      mapw -m "$1";
    fi
  }
  _runf map_wid "$@";
}

wm_hide () # Unmap a window
{
  hide_wid () {
    if wattr "$1" && wattr m "$1"; then
      mapw -u "$1";
    fi
  }
  _runf hide_wid "$@";
}

wm_ignore () # set a window's override_redirect property
{
  ignore_wid () {
    if wattr "$1" && ! wattr o "$1"; then
      ignw -s "$1";
    fi
  }
  _runf ignore_wid "$@";
}

wm_notice () # unset a window's override_redirect property
{
  notice_wid () {
    if wattr "$1" && wattr o "$1"; then
      ignw -r "$1";
    fi
  }
  _runf notice_wid "$@";
}

wm_raise () # raise a window to the top of the X stack
{
  raise_wid () {
    if wattr "$1"; then
      chwso -r "$1";
    fi
  }
  _runf raise_wid "$@";
}

wm_lower () # lower a window to the bottom of the X stack
{
  lower_wid () {
    if wattr "$1"; then
      chwso -l "$1";
    fi
  }
  _runf lower_wid "$@";
}

wm_kill () # kill a window
{
  kill_wid () {
    if wattr "$1"; then
      killwa "$1";
    fi
  }
  _runf kill_wid "$@";
}
