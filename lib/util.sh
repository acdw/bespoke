# util.sh
# some utility functions

# exit codes
SUCCESS=0; # Success!
E_EXE=126; # POSIX: Executable problem
E_FNF=127; # POSIX: File not found
E_ARG=100; # Argument doesn't make sense
E_WIN=101; # Can't find window
E_ERR=99;  # You've got 99 problems

say () # Better than echo
{ printf '%s\n' "$@"; }

die () # Fail gracefully
{
  err=$1; shift || exit $E_ERR;
  say "$@" >&2;
  exit $err;
}
