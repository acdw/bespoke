#!/bin/sh
# stack-manipulation functions

# a STACK is implemented as a file with ITEMs on each line
# the ITEMs can be whatever -- in what I'm thinking, they'll
# be windows, but in the future I'll have some with groups,
# etc.

# STACK OPERATIONS
stack_init () # Initiate a stack and echo its location, opt. cleaning it
{ # stack_init [-c] <stack...>
  CLEAN=false; [ "$1" = "-c" ] && { CLEAN=true; shift; }
  [ $# -gt 1 ] || die $E_ARG "stack: init: Not enough arguments";
  for stack; do
    dir="$(dirname "$(realpath "$stack")")";
    [ ! -d "$dir" ] && mkdir -p "$dir";
    if $CLEAN; then
      : > "$stack";
    else
      : >> "$stack";
    fi
    say "$stack";
  done
  unset -v dir CLEAN;
}

stack_kill () # Remove the stack file
{ # stack_kill <stack...>
  [ $# -gt 1 ] || die $E_ARG "stack: kill: Not enough arguments";
  for stack; do
    [ -f "$stack" ] && rm "$stack";
  done
}

# QUERY OPERATIONS
stack_ismember () # Returns whether item is in ANY of the given stacks
{ # stack_ismember <item> <stack...> # TODO add ALL option
  [ $# -ge 2 ] || die $E_ARG "stack: ismember: Not enough arguments";
  item="$1"; shift;
  cat "$@" | grep -q "$item";
  unset -v item;
}

stack_length () # Echos length of the stack
{ # stack_length <stack>
  [ $# -eq 1 ] || die $E_ARG "stack: length: Wrong number of args";
  [ -f "$1" ] || die $E_FNF "stack: length: Stack \"$stack\" not found";
  sed -n '$=' "$1";
}

# ITEM OPERATIONS
stack_remove () # Remove item(s) from the stack, optionally echoing them
{ # stack_remove [-p] <stack> <item...>|-<n>
  PRINT=false; [ "$1" = "-p" ] && { PRINT=true; shift; };
  [ $# -ge 2 ] || die $E_ARG "stack: remove: Not enough arguments";
  stack="$1"; shift;
  [ -f "$stack" ] || die $E_FNF "stack: remove: Stack \"$stack\" not found";
  t="$(mktemp /tmp/stackrm.XXXXXX)";
  case "$1" in
    (-[0-9]*) # Remove -n from end
      $PRINT && tail -n"${1#-}" "$stack";
      head -n"$1" "$stack" > "$t";
      mv "$t" "$stack";
      ;;
    (*) # Find items and remove them from the stack
      for item; do
        $PRINT && grep "$item" "$stack";
        grep -v "$item" "$stack" > "$t";
        # TODO find a more efficient way to remove multiple lines from a file
        mv "$t" "$stack";
      done
      ;;
  esac
  unset -v stack t PRINT;
}

stack_add () # Add item(s) to the stack (at the bottom), opt. uniqueness
{ # stack_add [-u] <stack> <item...>
  UNIQUE=false; [ "$1" = "-u" ] && { UNIQUE=true; shift; };
  [ $# -ge 2 ] || die $E_ARG "stack: add: Not enough arguments";
  stack="$1"; shift;
  [ -f "$stack" ] || stack_init "$stack";
  $UNIQUE && stack_remove "$stack" "$@";
  for item; do
    say "$item" >> "$stack";
  done
  unset -v stack UNIQUE;
}

stack_point () # Echo position of item in stack
{ # stack_point <stack> <item>
  [ $# -eq 2 ] || die $E_ARG "stack: point: Wrong number of args";
  [ -f "$1" ] || die $E_FNF "stack: point: Stack \"$stack\" not found";
  stack_ismember "$2" "$1" || return $?;
  sed "/$2/=;d" "$1";
}

# MANIPULATING STACKS
_stack_manipulate_args () # Boilerplate for manipulation functions
{
  func="$1"; shift;
  WRAP=false; [ "$1" = "-w" ] && { WRAP=true; shift; };
  [ $# -ge 2 ] || die $E_ARG "stack: $func: Not enough arguments";
  stack="$1"; item="$2"; n="${3:-1}"; l="$(stack_length "$stack")";
  [ $n -lt $l ] || die $E_ARG "stack: $func: n >= length";
  case "$item" in
    (-[0-9]*) p="${item#-}" ;;
    (*) p="$(stack_point "$stack" "$item")" ;;
  esac
}

stack_point_up () # Echo item above the given item in stack, opt. wrap
{ # stack_point_up [-w] <stack> <item>|-<p> [n=1]
  _stack_manipulate_args point_up "$@";
  # Manipulate
  p=$(( p - n ));
  if [ $p -lt 1 ]; then
    $WRAP && p=$(( p + l )) || p=1;
  fi
  sed "$p p;d" "$stack";
  unset -v stack item n l p;
}

stack_point_down () # Echo item below the given item in stack, opt. wrap
{ # stack_point_down [-w] <stack> <item>|-<p> [n=1]
  _stack_manipulate_args point_down "$@";
  # Manipulate
  p=$(( p + n ));
  if [ $p -gt $l ]; then
    $WRAP && p=$(( p - l )) || p=$l;
  fi
  sed "$p p;d" "$stack";
  unset -v stack item n l p;
}

_stack_shift () # Helper to shift item in a stack by one
{ # _stack_shift <up|down> # WRAP is set by caller
  case "$1" in
    (up)
      p=$(( p - 1 ));
      if [ $p -lt 1 ] && $WRAP; then
        sed '1d' "$stack" > "$t";
        sed '1p;d' "$stack" >> "$t";
      elif [ $p -lt 1 ]; then
        return 1;
      else
        sed -n "$p{h;n;G};p" "$stack" > "$t";
      fi
      ;;
    (down)
      if [ $p -ge $l ] && $WRAP; then
        sed '$p;d' "$stack" > "$t";
        sed '$d' "$stack" >> "$t";
      elif [ $p -ge $l ]; then
        return 1;
      else
        sed -n "$p{h;n;G};p" "$stack" > "$t";
      fi
      ;;
  esac 
}

stack_shift_up () # Shift given item up in stack, optionally wrapping
{ # stack_shift_up [-w] <stack> <item>|-<p> [n=1]
  _stack_manipulate_args shift_up "$@";
  # Sanity
  [ $l -ge 2 ] || die $E_ARG "stack: shift: fewer than 2 items in stack";
  # Manipulate
  t="$(mktemp /tmp/stackshift.XXXXXX)";
  while [ $n -gt 0 ]; do
    _stack_shift up || return 1;
    n=$(( n - 1 ));
    mv "$t" "$stack"; # TODO is there a more efficient way ???
  done
  unset -v stack item n l p t;
}

stack_shift_down () # Shift given item down in stack, optionally wrapping
{ # stack_shift_down [-w] <stack> <item>|-<p> [n=1]
  _stack_manipulate_args shift_down "$@";
  # Sanity
  [ $l -ge 2 ] || die $E_ARG "stack: shift: fewer than 2 items in stack";
  # Manipulate
  t="$(mktemp /tmp/stackshift.XXXXXX)";
  while [ $n -gt 0 ]; do
    _stack_shift down || return 1;
    n=$(( n - 1 ));
    mv "$t" "$stack";
  done
  unset -v stack item n l p t;
}
